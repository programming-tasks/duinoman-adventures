# Setup
To get the stuff you need, you should install virtualenv using pip `pip install virtualenv`
Then you need to make a folder for virtualenv (often called venv). `virtualenv venv`
When you have your venv folder, activate virtualenv using `source venv/bin/activate` or `venv\Scripts\activate` for Windows.
With virtualenv activated, install all the dependencies using `pip install -r requirements.txt`

To run the game, connect your custom made arduino controller to your computer (USB) and fire up Arduino IDE.
Inside Arduino IDE, upload the example code: StandardFirmata and locate the serialport your arduino is available at.
Then in game.py, put your serialport in `profirmata.talk_with_arduino_at("your_serialport")` instead of the one I use when debugging.
You can also change the pins for your joysticks in `profirmata.use_analog_pins(x1, y1, x2, y2)`
Then do `python3 game.py`

![in-game image](in-game.png)


### Day 1
Started to find out how to let python do stuff with an arduino. I Made a simple module (profirmata) that uses pyfirmata to do that stuff.
Had many problems with reading analog input and processing it, but got it working. When that worked I started learning pygame.


Using pygame I created a basic window and filled it with a blueish color. But then there were more problems with analog input.
When that was fixed I created duinoman in gimp and tried to display him in the game. At first he was very big so I needed to scale him down a lot.
Then I spent some time figuring out how coordinates, images and rects worked in pygame before trying to move duinoman.
I found out that I wanted the game to be fullscreen. So I did that too.


Moving duinoman was easy. I made a list called speed in the duinoman class that for the x-axis did: `self.rect.x += speed[0] * delta`.
If you set speed to a value it would constantly move duinoman regardless of current fps because of delta. The delta thing is the time between the current and previous frame.
Can be used to multiply speeds so that people with higher fps dont outrun people with lower fps.


### Day 2
This time I started to work on a background for the game in gimp.
When it was done, I spent a lot of time to figure out how to scale it to fit the screen without messing with the aspect ratio of the image.
I eventually figured it out and put it in the background. I tried to find good explanations for having it scroll infinitly, but didnt find anything at the time.


When the background worked, I started with gravity. It works with += gravity to a value constantly moving downwards.


That made duinoman fall, but forever. Therefore I defined the ground as the bottom of the screen so he would land on something. I didnt understand how to calculate the coordinates of the bottom of the screen yet.
As a temporary solution I let duinoman fall and exited the game when it looked like he wa standing on the bottom of the screen. Then I found the last console-print of the character position and used the y-value which was 829 px on my monitor.
Eventually I got him to stop falling, but when I tried to implement jumping he would just sag down beneath the edge of the screen when I tried to jump.


### Day 3
This time I started to fix the weird jumping by doing some changes and simplifying gravity (removing acceleration variable). After a while it worked and duinoman could move and jump.
Now I wanted to fix the hardcode ground at 829px (a problem for other resolutions and monitors). It turned out that the ground was screen height - character height.
The next thing was to make the background scroll infinitely. I could not figure it out myself being a noob with 2D games and pygame, but found tutorials on youtube.


When that worked I started to make a enemy/boss for duinoman. I looked around for competitors to the arduino uno from alternative brands.
I landed upon the MSP430 LaunchPad because of the similar shape and its red color. Then I made it a monstrousity in gimp and put him flying behind duinoman in the game.
Then I cnahged the controlls from normal movement to always running controls. The new idea was that the evil thing chases you with a lazer beam, and you must run and jump over obstacles.
While running you must shoot led dioes at him until his shield thing breaks and jump on his power supply/head when his shield is down. Perhaps 3 times. But not quite finished with this yet. Censored logos and things I dont own...


### Day 4
Cleaned up the code, finished comments and removed swp and temporary files. Was a little ambitious about the laser beam and diode blasters (Was also sick this week). The game became a endless runner...


### Day 5
Restructured code to make more sense. Made it easier to move the dude. Resized the evil thing and the buckets. The bucket is now black instead of white (easier to spot).
