import profirmata
import pygame
import random
import time
import sys

profirmata.talk_with_arduino_at("COM4")                                                     # Setting up pyfirmata to talk with the arduino at the specified serialport
profirmata.use_analog_pins([1, 0])                                                          # Defining wich pins I want to draw analog input from

pygame.init()                                                                               # Starting pygame
resolution = pygame.display.list_modes(depth=0, flags=pygame.FULLSCREEN)[0]                 # Getting highest resolution that supports fullscreen
print("Running with resolution ", resolution, "info", pygame.display.Info())                # Printing screen info
aspect = resolution[0] / resolution[1]                                                      # Calculating the screens aspect ratio

game_display = pygame.display.set_mode(resolution, pygame.FULLSCREEN)                       # Setting pygame display resolution
LOGO_IMAGE = pygame.image.load("assets/logo.png")                                           # Loading the logo
pygame.display.set_icon(LOGO_IMAGE)                                                         # Displaying logo
pygame.display.set_caption("Duinoman Adventures")                                           # Setting pygame window title
game_clock = pygame.time.Clock()                                                            # Making game clock to manage fps and other tick related things
delta = 0                                                                                   # Defining variable to hold time between frames
fps = 60                                                                                    # Setting desired FPS

class duinoman(pygame.sprite.Sprite):                                                       # Defining duinoman, the main character
    def __init__(self):                                                                     # Initializing a new class
        pygame.sprite.Sprite.__init__(self)

        self.image = DUINOMAN_IMAGE                                                         # Setting the sprite image to the duinoman image in the assets folder
        self.size = self.image.get_size()                                                   # Setting duinomans size to be as big as the duinoman image
        self.image = pygame.transform.scale(self.image, (int(self.size[0] / 3.5), int(self.size[1] / 3.5))) # Making the image a lot smaller

        self.rect = self.image.get_rect()                                                   # Setting the sprite rect to be as big as the (new) image size
        self.rect.center = (resolution[0] // 2, resolution[1] // 2)                         # Initially positioning the player at the center of the screen


        self.speed = [0, 0]                                                                 # Defining the players speed variable (used to move)
        self.is_on_ground = False                                                           # Defining variable to know if the player is standing or falling
        self.can_jump = False                                                               # Defining variable to allow jumping or not

    def update(self):
        self.rect.x += int(self.speed[0] * delta)                                           # "Top-Down" movement for x-axis
        self.rect.y += int(self.speed[1] * delta)                                           # Same for y

        if self.rect.y > resolution[1] - self.image.get_size()[1] - 7:                      # Detecting if duinoman is at the bottom of the screen (ground)
            self.is_on_ground = True
        else:
            self.is_on_ground = False

        if self.rect.colliderect(enemy.rect):                                               # If the enemy collides with the player, exit.
            die()

class evil_thing(pygame.sprite.Sprite):                                                     # Defining the evil thing
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)                                                 # Initializing new class
        self.original_image = ENEMY_IMAGE                                                   # Setting image to be the enemy image
        self.size = self.original_image.get_size()                                          # Getting image size
        self.original_image = pygame.transform.scale(self.original_image, (int(self.size[0] * 0.45), int(self.size[1] * 0.45))) # Scaling image
        self.rect = self.original_image.get_rect(center=(resolution[0] // 5, resolution[1] // 2))    # Moving image to 1/5 of screen at x and 1/2 of screen at y

        self.constant_rotation = 0                                                          # Defining two variables to hold constant rotation
        self.rotation = 0                                                                   # Defining variable to hold actual rotation

    def update(self):
        self.rotation += self.constant_rotation                                             # constantly increasing rotation variable
        self.image = pygame.transform.rotate(self.original_image, self.rotation)            # Rotating a new copy of the original image

class bucket(pygame.sprite.Sprite):                                                         # Making a hazardous water bucket
    def __init__(self):                                                                     # initing as before
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.transform.scale(BUCKET_IMAGE, (int(BUCKET_IMAGE.get_size()[0] * 0.45), int(BUCKET_IMAGE.get_size()[1] * 0.45))) # Scaling image
        self.rect = self.image.get_rect()                                                   # Aquiring a rect from the image
        self.rect.center = (int(resolution[0] + self.image.get_size()[0] / 2), int(resolution[1] - self.image.get_size()[1] / 2)) # Moving rect just outside the screen
        self.rect[1] += 15

    def update(self):
        self.rect.x -= int(game_speed_multiplier * 1.2 * delta)                             # Accelerating image a bit faster than background (paralax) to the left
        if self.rect.x <= -self.image.get_size()[0]:                                        # If the image has moved completely outside the screen to the left...
            game_sprites.remove(self)                                                       # Remove it
        if self.rect.colliderect(player.rect):                                              # If it collides with the player, exits
            die()

def die():
    print("EPIC GAMER FAIL...")
    print("Your score was", score)
    sys.exit()

background_image = pygame.image.load("assets/sprites/background.png").convert_alpha()       # Storing background_image
background_image = pygame.transform.scale(background_image, (resolution[0], int(resolution[0] / aspect))) # Scaling background image to fit display
ENEMY_IMAGE = pygame.image.load("assets/sprites/launchpad_enemy.png").convert_alpha()
DUINOMAN_IMAGE = pygame.image.load("assets/sprites/duinoman.png").convert_alpha()           # Loading duino man image for the player sprite
BUCKET_IMAGE = pygame.image.load("assets/sprites/hazard.png").convert_alpha()
background_color = (125, 255, 255)                                                          # Storing the background_color in a tuple

game_speed_multiplier = 256.0                                                               # Controlling how fast the game runs at the beginning
jump_force = 1200                                                                           # Controlling how much force is used to jump
gravity = 9.8 * 3                                                                           # Defining how strong gravity is
game_start_time = time.time()
reset_time = time.time()                                                                    # Initial time or time at last time reset
elapsed_time = 0                                                                            # Variable to keep track of elapsed time since last time reset
random_time = 5                                                                             # Time used to spawn buckets. if elapsed_time > random_time -> do stuff
scroll_pos = 0

game_sprites = pygame.sprite.Group()                                                        # Making a sprite group to keep drawing code clean
player = duinoman()                                                                         # Creating a player out of duinoman
enemy = evil_thing()                                                                        # Making an enemy out of evil_thing
game_sprites.add(player)                                                                    # Adding player to my sprite group
game_sprites.add(enemy)                                                                     # Same for enemy

run = True                                                                                  # Starting the game loop
while run:
    if delta > 0.5:                                                                         # Checking if delta is greater OK values. This takes care of lag spike bugs
        delta = 1 / fps
        analog_input[0] = 0                                                                 # If lag spikes occur we pretend to have normal fps.
    for event in pygame.event.get():
        if event.type == pygame.QUIT:                                                       # Quit if you somehow pressed quit
            run = False
        if event.type == pygame.KEYDOWN:                                                    # Or Escape...
            if event.key == pygame.K_ESCAPE:
                run = False

    analog_input = profirmata.get_analog_input()                                            # Getting analog input in order profirmata.use_analog_pins([your pin order])
    analog_input[0] = profirmata.map(analog_input[0], 0.0, 1.0, 1.0, -1.0)                  # Mapping input from joystick 1 to useful value-ranges
    analog_input[1] = profirmata.map(analog_input[1], 0.0, 1.0, -1.0, 1.0)                  # Mapping joystick 2 (Not used here)

    player.speed[0] = analog_input[0] * 750

    player.can_jump = True                                                                  # Saying that the player can jump as long he is not faling or jumping
    if not player.is_on_ground:
        player.can_jump = False
        player.speed[1] += gravity

    else:
        player.speed[1] = 0.0

        if analog_input[1] < -0.5 and player.can_jump:
            player.speed[1] = -jump_force
            player.can_jump = False 														# Rotating the enemy

    elapsed_time = time.time() - reset_time                                                 # Calculating elapsed time since start or last reset.
    if elapsed_time > random_time:                                                          # If elapsed time is greater than random time; then spawn a water-bucket
        game_sprites.add(bucket())
        reset_time = time.time()                                                            # Reset spawn time to not instantly spawn new buckets
        random_time = random.uniform(2.0, 5.0)                                              # Get a new random time to not spawn buckets at the same rate

    game_sprites.update()                                                                   # Update all the game sprites
    game_display.fill(background_color)                                                     # Coloring the background of the game (helps with not killing your vram)

    scroll_pos -= game_speed_multiplier * delta                                             # Moving the background to the left
    bg_rel_x = int(scroll_pos % background_image.get_rect().width)                          # Making variable that resets itself after scrolling as long as resolution
    game_display.blit(background_image, (bg_rel_x - background_image.get_rect().width, 0))  # Displaying the scrolling background
    if bg_rel_x < resolution[0]:                                                            # If the image has moved outside the display, add another to fill the gap
        game_display.blit(background_image, (bg_rel_x, 0))                                  # Show the new image
                                                                                            # This stuff creates the illusion of an endless background (mod-magic)
    game_sprites.draw(game_display)                                                         # Draw all the game sprites

    score = abs(int(time.time() - game_start_time + game_speed_multiplier / 10))            # Calculating a score based on time and acceleration from the game-speed
    font = pygame.font.Font(pygame.font.get_default_font(), 24)                             # Getting a font with size 24
    score_text = font.render(str(score), 1, (255, 255, 255))                                # Making the score into text
    game_display.blit(score_text, (0, 0))                                                   # Displaying the text

    game_speed_multiplier += 0.5                                                            # Slowly accelerating the game speed
    enemy.constant_rotation += 0.001                                                        # increasing rotation-rate for the evil thing as well

    pygame.display.update()                                                                 # Updating all the visuals of the game
    delta = game_clock.tick(fps) / 1000                                                     # Making the game tick at the specified fps

	# Debug
    #print("Analog input:", analog_input, "\tPlayer speed:", player.speed, "\tPlayer position:", (player.rect.x, player.rect.y), "\tDelta:", delta)
    #print("Start time:", reset_time, "\tRandom time:", random_time, "\tElapsed time:", elapsed_time)
